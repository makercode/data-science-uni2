import numpy as np
import matplotlib.pyplot as plt
x = np.linspace(-np.pi,np.pi,100)
print(x)
y = np.sin(x)
plt.plot(x,y)
plt.grid(True)
plt.show()